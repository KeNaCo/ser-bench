Ser Bench
=========

Is a serialisation/deserialization benchamarking tool for multiple protocols and implementations comparison.

Setup
_____

Benchmark requirements:

``pip install -r requirements.txt``

To select required protocols, edit *benchmark-requirements.txt*, then:

``pip install -r serializers-requirements.txt``

.. warning::

    Some libraries in profiles may not be installable from pypi.

Run
___

As simple as can be:

``python bench2.py``

Credits
_______

All my work is based on `this gist <https://gist.github.com/cactus/4073643>`_.
