"""
This is adapter module to xmlrpc.client library
"""
from xmlrpc.client import loads
import xmlrpc.client


def dumps(data):
    return xmlrpc.client.dumps((data,))
