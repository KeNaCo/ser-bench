import csv
import os
import datetime
from collections import namedtuple
from functools import partial
from timeit import timeit
from typing import Callable, Optional, Dict, Iterable, Any

import pandas as pd
from chartify import Chart
from faker import Faker
from halo import Halo


class Profile:
    def __init__(self, name: str, module_name: str,
                 loader_options: Optional[Dict] = None,
                 dumper_options: Optional[Dict] = None):
        self.name = name
        self.module_name = module_name
        self.loader_options = loader_options or {}
        self.dumper_options = dumper_options or {}

        self._test_method: Callable = None

    def dumper(self) -> Callable:
        """ Import module and create partial dumper function """
        module = __import__(self.module_name, globals(), locals())
        return partial(module.dumps, **self.dumper_options)

    def loader(self) -> Callable:
        """ Import module and create partial loader function """
        module = __import__(self.module_name, globals(), locals())
        return partial(module.loads, **self.loader_options)

    def set_dumper(self) -> 'Profile':
        """ Set dumper as test method """
        self._test_method = self.dumper()
        return self

    def set_loader(self) -> 'Profile':
        """ Set loader as test method """
        self._test_method = self.loader()
        return self

    def timeit(self, payload, number):
        """ Setup and start profiling via timeit """
        run_test = self._test_method

        return timeit("run_test(payload)", number=number, globals=locals())


Payload = namedtuple('Payload', ['name', 'data'])
Measurement = namedtuple('Measurement', ['profile_name', 'payload_name', 'size', 'dumps_time', 'loads_time'])


def benchmark(profiles: Iterable[Profile], payloads: Iterable[Payload], number=10000) -> Iterable[Measurement]:
    """ Benchmarking all combination of profiles and payloads that not raise error """
    i = 0
    i_max = len(profiles) * len(payloads)
    status = lambda i: 'Benchmarking... {}/{}'.format(i, i_max)

    spinner = Halo(text=status(i), spinner='dots')
    spinner.start()

    measurements = []
    for profile in profiles:
        for payload in payloads:
            i += 1
            spinner.text = status(i)

            try:
                loads_payload = profile.dumper()(payload.data)
                size = len(loads_payload)
                dumps_time = profile.set_dumper().timeit(payload.data, number)
                loads_time = profile.set_loader().timeit(loads_payload, number)
                measurements.append(Measurement(profile.name, payload.name, size, dumps_time, loads_time))
            except (ModuleNotFoundError, TypeError, ValueError, OverflowError, Exception) as e:  # FIXME Exception should be only temporary!!!
                spinner.fail('Skip {}:{} {}'.format(profile.name, payload.name, e))
                spinner.start()

    spinner.stop()
    return measurements


def store_csv(measurements: Iterable[Measurement], directory, file_name):
    """ Store data as csv file """
    file_path = os.path.join(directory, file_name)
    with open(file_path, 'w') as f:
        w = csv.writer(f)
        w.writerow(('profile_name', 'payload_name', 'size', 'dumps_time', 'loads_time'))
        w.writerows([(m.profile_name, m.payload_name, m.size, m.dumps_time, m.loads_time) for m in measurements])

    print(f'CSV stored in {file_path}')


def create_graph(data, measured_column: str, measured_unit: str, title: str) -> Chart:
    """ Create comparison graph of one measued column """
    chart = Chart(blank_labels=True, x_axis_type='categorical')
    chart.set_title(title)
    chart.axes.set_yaxis_label(f'[{measured_unit}]')
    chart.axes.set_xaxis_tick_orientation('vertical')
    chart.figure.width = 1280
    chart.plot.bar(
        data_frame=data,
        categorical_columns=['payload_name', 'profile_name'],
        numeric_column=measured_column,
        color_column='profile_name'
    )
    return chart


def graph_measurements(directory, filename):
    """ Graph all measurements """
    data = pd.read_csv(os.path.join(directory, filename))
    size_graph = create_graph(data, measured_column='size', measured_unit='b', title='Size comparison')
    size_graph.save(filename=os.path.join(directory, 'size_graph.html'))
    loads_graph = create_graph(data, measured_column='loads_time', measured_unit='s', title='Loads time comparison')
    loads_graph.save(filename=os.path.join(directory, 'loads_graph.html'))
    dumps_graph = create_graph(data, measured_column='dumps_time', measured_unit='s', title='Dumps time comparison')
    dumps_graph.save(filename=os.path.join(directory, 'dumps_graph.html'))

    print(f'Graphs stored under {directory}')


def fake_nested_dict_with_str_keys_only(value_types) -> Dict:
    """ Name of function say it all """
    def key_to_str(dict_: Dict) -> Dict[str, Any]:
        """ Create new dict with str keys """
        ret_dict = {}
        for key, value in dict_.items():
            ret_dict[str(key)] = value if not isinstance(value, dict) else key_to_str(value)
        return ret_dict

    fake_dict = fake.pystruct(10, *value_types)[2]  # pystruct returns tuple, list, simple dict, nested dict
    return key_to_str(fake_dict)


if __name__ == '__main__':
    profiles = (
        Profile('json', 'json'),
        Profile('simplejson', 'simplejson'),
        Profile('ujson', 'ujson'),
        Profile('msgpack', 'msgpack'),
        Profile('cbor', 'cbor'),
        Profile('marshal', 'marshal'),
        Profile('pickle', 'pickle'),
        Profile('pickle-v2', 'pickle', dumper_options={'protocol': 2}),
        Profile('xmlrpc', 'xmlrpc_adapter'),
        Profile('frpc-v1', 'frpc', dumper_options={'version': 1}, loader_options={'version': 1}),
        Profile('frpc-v2', 'frpc', dumper_options={'version': 2}, loader_options={'version': 2}),
        Profile('frpc-v3', 'frpc', dumper_options={'version': 3}, loader_options={'version': 3}),
        Profile('fastrpc-v1', 'fastrpc_adapter', dumper_options={'useBinary': True, 'protocolVersionMajor': 1, 'protocolVersionMinor': 0}, loader_options={'useBinary': True}),
        Profile('fastrpc-v2', 'fastrpc_adapter', dumper_options={'useBinary': True, 'protocolVersionMajor': 2, 'protocolVersionMinor': 0}, loader_options={'useBinary': True}),
        Profile('fastrpc-v3', 'fastrpc_adapter', dumper_options={'useBinary': True, 'protocolVersionMajor': 3, 'protocolVersionMinor': 0}, loader_options={'useBinary': True}),
    )

    fake = Faker()
    # we need to remove decimal and datetime type from default types due to lot of serialization errors
    value_types = ['str', 'str', 'str', 'str', 'float', 'int', 'int', 'uri', 'email']
    payloads = (
        Payload('int', fake.pyint()),
        Payload('negative integer', -fake.pyint()),
        Payload('float', fake.pyfloat()),
        Payload('bool', fake.pybool()),
        Payload('str (max=50)', fake.pystr(max_chars=50)),
        Payload('tuple', fake.pytuple(10, True, *value_types)),
        Payload('list', fake.pylist(10, True, *value_types)),
        Payload('dict (simple)', fake.pydict(10, True, *value_types)),
        Payload('dict (nested)', fake.pystruct(10, *value_types)[2]),  # pystruct returns tuple, list, simple dict, nested dict
        Payload('dict (nested, str keys only)', fake_nested_dict_with_str_keys_only(value_types))
    )

    measurements = benchmark(profiles, payloads)
    directory_path = './benchmarks/{}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    os.makedirs(directory_path)
    filename = 'data.csv'
    store_csv(measurements, directory_path, filename)
    graph_measurements(directory_path, filename)

