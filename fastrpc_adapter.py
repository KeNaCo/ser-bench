"""
This is adapter module to fastrpc library
"""
import fastrpc


def dumps(data, **kwargs):
    return fastrpc.dumps((data,), **kwargs)  # fastrpc dumper needs tuple, but dumps only first item


def loads(data, **kwargs):
    return fastrpc.loads(data, **kwargs)[0]  # fastrpc loader returns tuple for some reason
